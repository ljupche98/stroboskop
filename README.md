# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
git clone https://ljupche98@bitbucket.org/ljupche98/stroboskop.git
```

Naloga 6.2.3:
https://bitbucket.org/ljupche98/stroboskop/commits/8ddf3a260e2e87cec1b0e977dfd88e8da0bb7420

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
https://bitbucket.org/ljupche98/stroboskop/commits/f921da99a13e2bd3673a02e5bf3a6adef6cd1fb4

Naloga 6.3.2:
https://bitbucket.org/ljupche98/stroboskop/commits/d72d9b114cd80c535a907648a5aeb738d71b997b

Naloga 6.3.3:
https://bitbucket.org/ljupche98/stroboskop/commits/943eabbb94b8ece1eac91843c84feab706803d45

Naloga 6.3.4:
https://bitbucket.org/ljupche98/stroboskop/commits/cc26f3b09cc4543032b510fff143dac6a8dc91e2

Naloga 6.3.5:

```
git checkout master
git merge izgled
git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
https://bitbucket.org/ljupche98/stroboskop/commits/84ed833e88881adf76acdb1c78963d0a0b3c7c4e

Naloga 6.4.2:
https://bitbucket.org/ljupche98/stroboskop/commits/5714fab885fc3e66b7c4af988e168a8bef3cd91a

Naloga 6.4.3:
https://bitbucket.org/ljupche98/stroboskop/commits/880b9795734e6a75d7069ca48be577e9db936301

Naloga 6.4.4:
https://bitbucket.org/ljupche98/stroboskop/commits/f0ce2b636926c4c0f99e5ab02508bb4e2451d54d